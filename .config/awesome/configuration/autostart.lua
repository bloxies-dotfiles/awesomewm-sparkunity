local apps = require('configuration.apps')
local configuration = require('configuration.config')
local filesystem = require('gears.filesystem')

-- List of apps to start once on start-up
return {
  run_on_start_up = {

    'python3 ~/.config/awesome/configuration/utils/hud-menu-service.py', -- The daemon that is required for the HUD Menu to work

    'picom --config ' .. filesystem.get_configuration_dir() .. 'configuration/picom.conf', -- Compositor TODO Make make a low graphics mode that disables this compositing and other components

    'udiskie', -- Removable Media Manager (Automounting etc.)

    'nm-applet --indicator', -- wifi
    --'blueberry-tray', -- Bluetooth tray icon

    'light-locker',             -- Screensaver and Locker

    'xfce4-power-manager',  -- XFCE's Power manager
    -- 'powerkit',                 -- Power manager

    'numlockx on', -- enable numlock

    -- '/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)', -- credential manager (alternate directory if the first one is incorrect)
    '/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1  & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)', -- credential manager
    --'/usr/lib/x86_64-linux-gnu/libexec/polkit-kde-authentication-agent-1 & eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)', -- credential manager

    -- 'blueman-tray', -- bluetooth tray

    'gxkb',                     -- Simple language switcher

    "xcape -e 'Super_L=Alt_L|F1' && xcape -e 'Alt_L=Alt_L|F2'", -- Bind superkey to dashpanel toggle and then Bind alt key to HUDMenu toggle
    "xcape -e 'Alt_L=Alt_L|F2'", -- Bind alt key to HUD Menu toggle

    'volctl',                   -- Simple volume mixer

    'redshift-gtk',

    -- Add applications that need to be killed between reloads
    -- to avoid multipled instances, inside the awspawn script
    '~/.config/awesome/configuration/autostartonce.sh' -- Spawn "dirty" apps that can linger between sessions

  }
}
