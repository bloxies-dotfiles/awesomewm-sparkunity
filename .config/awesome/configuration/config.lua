local dpi = require("beautiful.xresources").apply_dpi
local awful = require('awful')

-- vw/vh are used when you want certain elements to scale nicely/the same on all monitors/resolutions.
-- 1 vw unit = Relative to 1% of the width of the screen it is targetting
-- 1 vh unit = Relative to 1% of the height of the screen it is targetting
awful.screen.connect_for_each_screen(function(s)

    function vw(width)
      vw = (s.geometry.width / 100) * width
      return vw;
    end

    function vh(height)
      vh = (s.geometry.height / 100) * height
      return vh;
    end

end)

-- You can store common configuration variables here, or completely get rid of this file.
local configuration = {

  -- Height of the top panel
  toppanel_height      = dpi(25),

  -- Position of the top panel "left" , "right" , "top" , "bottom"
  toppanel_position    = "top",

  -- Width of the side panel
  sidepanel_width      = dpi(67),

  -- Position of the top panel "left" , "right" , "top" , "bottom"
  sidepanel_position    = "left",

  -- Height of the dashpanel

  -- If you want it to scale nicely on all resolutions, you can use this.
  -- dashpanel_height = vh(57.8),
  dashpanel_height = dpi(669),

  -- Width of the dashpanel

  -- If you want it to scale nicely on all resolutions, you can use this.
  -- dashpanel_width = vw(49.2),
  dashpanel_width = dpi(1173),

}

return configuration
