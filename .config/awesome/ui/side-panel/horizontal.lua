local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')
require('widgets.side-panel.horizontal')

local SidePanel = function(s, side_panel_base)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        ontop = true,
        screen = s,
        type = "dock",
        width = side_panel_base.width,
        height = side_panel_base.height,
        x = side_panel_base.x,
        y = side_panel_base.y,
        bg = "#00000000",
        fg = beautiful.side_panel_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )

  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  panel:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      {
        mylauncher,

        margins = 9,
        widget = wibox.container.margin,
      },
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      {
        s.mylayoutbox,

        margins = 9,
        widget = wibox.container.margin,
      },
    },
  }


  return panel
end

return SidePanel

