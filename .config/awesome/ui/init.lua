local awful = require('awful')
local gears = require('gears')

-- These are the base panels, you can toggle them off but it is not recommended.
dash_panel = require('ui.dash-panel')
top_panel_base = require('ui.top-panel')
side_panel_base = require('ui.side-panel')

-- Modules for the top panel (You can enable/disable them here)
netbook_mode = require('modules.top-panel.netbook')

-- Modules for the side panel (You can enable/disable them here)
-- autohide_trigger = require('modules.side-panel.autohide')
side_panel_border = require('modules.side-panel.border')

-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(
  function(s)

    -- You can get edit/rid of this conditional if you want certain bars on specific screens or all screens etc.
    if s.index == 1 then

      -- Panels
      -- The conditionals check if the function exists before calling it, suppressing errors.

      -- Top Panel
      if type(top_panel) == "function" then
        s.top_panel_base = top_panel_base(s)
        s.top_panel = top_panel(s, s.top_panel_base)
      end

      if type(netbook_mode) == "function" then
        netbook_mode(s, s.top_panel, s.top_panel_base)
      end

      -- Side Panel

      if type(side_panel) == "function" then
        s.side_panel_base = side_panel_base(s)
        s.side_panel = side_panel(s, s.side_panel_base)
      end

      -- We don't need to get the s.autohide_trigger because we just want to check if the base function exists.
      if type(side_panel_border) == "function" then
        s.side_panel_border = side_panel_border(s, s.side_panel, autohide_trigger)
      end

      if type(autohide_trigger) == "function" and type(side_panel_border) == "function" then
        s.autohide_trigger = autohide_trigger(s, s.side_panel, s.side_panel_base, s.side_panel_border)
      elseif type(autohide_trigger) == "function" then
        s.autohide_trigger = autohide_trigger(s, s.side_panel, s.side_panel_base, false)
      end

      -- Dash Panel
      if type(dash_panel) == "function" then
        s.dash_panel = dash_panel(s, s.top_panel, s.top_panel_base, s.side_panel, s.side_panel_base, s.side_panel_border)
      end

    end

  end
)

-- Hide bars when app go fullscreen
function updateBarsVisibility()
  for s in screen do
    if s.selected_tag then
      local fullscreen = s.selected_tag.fullscreenMode

      -- These are the bars that are hidden when on any fullscreen mode (The awesomewm fullscreen mode and app fullscren modes like youtube)
      -- If you want bars to be invisible when you fullscreen an app, you can do so like so :
      -- The conditionals check if the function exists before calling it, suppressing errors.
      if type(top_panel) == "function" then
        s.top_panel_base.visible = not fullscreen
        s.top_panel.visible = not fullscreen
      end

      if type(side_panel) == "function" and type(autohide_trigger) ~= "function" then
        s.side_panel_base.visible = not fullscreen
        s.side_panel.visible = not fullscreen
      else
        s.side_panel_base.visible = false
        s.side_panel.visible = false
      end

      if type(autohide_trigger) == "function" then
        s.autohide_trigger_base.visible = not fullscreen
        s.autohide_trigger.visible = not fullscreen
      end


      -- If you want bars to be visible even when you fullscreen an app, you can do it like so :
      -- s.top_panel.visible = visible

      -- I'm sure you can figure out other things you can do here.
    end
  end
end

_G.tag.connect_signal(
  'property::selected',
  function(t)
    updateBarsVisibility()
  end
)

_G.client.connect_signal(
  'property::fullscreen',
  function(c)
    c.screen.selected_tag.fullscreenMode = c.fullscreen
    updateBarsVisibility()
  end
)

_G.client.connect_signal(
  'unmanage',
  function(c)
    if c.fullscreen then
      c.screen.selected_tag.fullscreenMode = false
      updateBarsVisibility()
    end
  end
)
