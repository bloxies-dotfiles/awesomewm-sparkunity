local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')
require('widgets.top-panel.vertical')

local TopPanel = function(s, top_panel_base)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        ontop = true,
        screen = s,
        type = "dock",
        width = top_panel_base.width,
        height = top_panel_base.height,
        x = top_panel_base.x,
        y = top_panel_base.y,
        bg = "#00000000",
        fg = beautiful.top_panel_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )

  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  panel:setup {
    layout = wibox.layout.align.vertical,
    { -- Left widgets
      layout = wibox.layout.fixed.vertical,
      s.mytaglist,

      {
        {
          layout = wibox.layout.fixed.vertical,
          killbutton,
          maximizebutton,
          minimizebutton,
          stickybutton,
          ontopbutton,
          s.mypromptbox,
          visible = false,
          id = "clientbuttons",
        },
        margins = 6,
        widget = wibox.container.margin
      },
    },
    nil, -- Middle Widget
    { -- Right widgets
      layout = wibox.layout.fixed.vertical,
      mykeyboardlayout,
      wibox.widget.systray(),
      mytextclock,
    },
  }




  return panel
end

return TopPanel

