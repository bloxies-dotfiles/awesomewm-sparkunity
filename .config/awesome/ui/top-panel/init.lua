local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

if configuration.toppanel_position == "top" or configuration.toppanel_position == "bottom"  then
  top_panel = require('ui.top-panel.horizontal')
else
  top_panel = require('ui.top-panel.vertical')
end

-- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code

local TopPanelBase = function(s)

  local panel =
    awful.wibar(
      {
        ontop = true,
        screen = s,
        type = "toolbar",
        position = configuration.toppanel_position,
        width = configuration.toppanel_height,
        height = configuration.toppanel_height,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = true,
        bg = beautiful.top_panel_bg,
        fg = beautiful.top_panel_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )


  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  return panel

end

return TopPanelBase
