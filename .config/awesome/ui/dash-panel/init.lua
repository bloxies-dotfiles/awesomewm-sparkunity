local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')
require('widgets.side-panel.horizontal')

local DashPanel = function(s, top_panel, top_panel_base, side_panel, side_panel_base, side_panel_border)

  function dash_shape
    (cr, width, height)

    -- If you're curious about the values 8 and 4, they are based off of how long each curve is in the Unity asset. You can find it in the themes/chameleon/dash folder
    -- Top Left
    cr:move_to(0 , 0)

    -- Top Right
    cr:line_to(width , 0)

    -- Top Right - Bottom Right Corner of Toppanel
    cr:line_to(width , configuration.toppanel_height)

    -- We have this line to make sure the curve doesn't start before this
    cr:line_to( (configuration.sidepanel_width + configuration.dashpanel_width + 12) , configuration.toppanel_height)

    -- Curve 1
    cr:curve_to( (configuration.sidepanel_width + (configuration.dashpanel_width + 12)) , configuration.toppanel_height, (configuration.sidepanel_width + configuration.dashpanel_width) , configuration.toppanel_height , (configuration.sidepanel_width + configuration.dashpanel_width) , (configuration.toppanel_height + 12) )

    -- Make sure the curve stops right here
    cr:line_to( (configuration.sidepanel_width + configuration.dashpanel_width) , (configuration.toppanel_height + 12) )

    -- Prepare for next curve, defining where it starts.
    cr:line_to( (configuration.sidepanel_width + configuration.dashpanel_width) , (configuration.dashpanel_height - 4) )

    -- Curve 2
    cr:curve_to( (configuration.sidepanel_width + configuration.dashpanel_width) , (configuration.dashpanel_height - 4), (configuration.sidepanel_width + configuration.dashpanel_width) , configuration.dashpanel_height , (configuration.sidepanel_width + configuration.dashpanel_width - 4) , configuration.dashpanel_height)

    -- Define where curve ends.
    cr:line_to( (configuration.sidepanel_width + configuration.dashpanel_width - 4) , configuration.dashpanel_height )

    -- Dashpanel Side, leaving space for the corner
    cr:line_to( (configuration.sidepanel_width + configuration.dashpanel_width - 4) , configuration.dashpanel_height )

    -- Dashpanel Bottom, making sure to leave space for the corner
    cr:line_to( (configuration.sidepanel_width + 12) , configuration.dashpanel_height )

    -- Curve 3
    cr:curve_to( (configuration.sidepanel_width + 12) , configuration.dashpanel_height, configuration.sidepanel_width , configuration.dashpanel_height , configuration.sidepanel_width , (configuration.dashpanel_height + 12) )

    -- Make sure the curve stops right here
    cr:line_to( configuration.sidepanel_width , (configuration.dashpanel_height + 12) )

    -- Bottom Right Corner of Sidepanel
    cr:line_to( configuration.sidepanel_width , height )

    -- Bottom Left Corner of Sidepanel
    cr:line_to( 0 , height )

    cr:close_path()
    
  end

  -- dashpanel = wibox({ shape = dash_shape, width = s.geometry.width, height = s.geometry.height , visible = false, ontop = true, x = 0, y = 0, type = "dock"})
  -- dashpanel.bg = beautiful.bg_dash_transparent


  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        shape = dash_shape,
        ontop = true,
        screen = s,
        type = "dock",
        width = s.geometry.width,
        height = s.geometry.height,
        x = 0,
        y = 0,
        -- bg = beautiful.side_panel_bg,
        bg = beautiful.dash_panel_bg,
        fg = beautiful.dash_panel_fg,
        visible = false,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )

  function dashpanel_toggle()

    -- The spawning of the actual panel launcher (rofi is handled via bash in the keybindings file)
    if panel.visible then
      panel.visible = false
      -- We may need a check to see if the text is black and then change it to white so it is readable on transparent surface
      top_panel_base.opacity = 1
      side_panel_base.opacity = 1
    else
      panel.visible = true

      if not client.focus or not client.focus.maximized then
        top_panel_base.opacity = 0
      end

      side_panel_base.opacity = 0

      top_panel.visible = false
      side_panel.visible = false
      side_panel_border.visible = false
      gears.timer {
        timeout   = 0.2,
        autostart = true,
        single_shot  = true,
        callback  = function()
          top_panel.visible = true
          side_panel.visible = true
          side_panel_border.visible = true

        end
      }


    end

  end

  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  -- panel:setup {
  --   layout = wibox.layout.align.horizontal,
  --   { -- Left widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     {
  --       mylauncher,

  --       margins = 9,
  --       widget = wibox.container.margin,
  --     },
  --   },
  --   s.mytasklist, -- Middle widget
  --   { -- Right widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     {
  --       s.mylayoutbox,

  --       margins = 9,
  --       widget = wibox.container.margin,
  --     },
  --   },
  -- }


  return panel
end

return DashPanel

