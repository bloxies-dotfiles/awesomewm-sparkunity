local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')

clientname = wibox.widget{
  text = 'Desktop',
  align  = 'center',
  valign = 'center',
  font   = 'Ubuntu Bold 10',
  widget = wibox.widget.textbox
}

-- This code automatically checks the lsb_release for the name of your distribution, which is then used for the client name.
awful.spawn.easy_async_with_shell([[{ lsb_release -is ; echo "Desktop"; } | tr "\n" " "]], function(stdout, stderr, reason, exit_code)

                                    -- For some reason I cannot concatenate onto end of the string? So therefore I concatenated via bash instead.
                                    desktop = stdout

                                    clientname.text = desktop

                                    -- Check if window is updated and change the name
                                    client.connect_signal("focus", function(c)

                                                            if c then
                                                              -- Perhaps it would be better to pcall this and combine the two conditionals
                                                              local strout = c.class:gsub("^%l", string.upper)

                                                              if clientname.text ~= strout then
                                                                clientname.text = strout
                                                              end

                                                            end

                                    end)

                                    client.connect_signal("unfocus", function(c)
                                                            -- do something
                                                            if c then
                                                              clientname.text = desktop
                                                            end

                                    end)

end)


