local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

killbutton = wibox.widget {
  image  = beautiful.titlebar_close_button_focus,
  resize = true,
  widget = wibox.widget.imagebox
}

killbutton:connect_signal("clientbutton::kill", function()
                        local c = client.focus
                        if c then
                          c:kill()
                        end

end)

killbutton:connect_signal("button::press", function()
                            killbutton:emit_signal("clientbutton::kill")
                            killbutton.image = beautiful.titlebar_close_button_focus_press
end)

killbutton:connect_signal("button::release", function()
                            killbutton.image = beautiful.titlebar_close_button_focus
end)

killbutton:connect_signal("mouse::enter", function()
                            killbutton.image = beautiful.titlebar_close_button_focus_hover
end)

killbutton:connect_signal("mouse::leave", function()
                            killbutton.image = beautiful.titlebar_close_button_focus
end)

