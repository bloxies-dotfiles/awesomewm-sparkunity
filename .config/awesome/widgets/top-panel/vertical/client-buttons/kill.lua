local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

killbutton = wibox.widget {
  image  = beautiful.titlebar_close_button_focus,
  resize = true,
  widget = wibox.widget.imagebox
}

killbutton:connect_signal("clientbutton::kill", function()

                        local c = client.focus
                        if c then
                          c:kill()
                        end

end)

killbutton:connect_signal("button::press", function()
                        killbutton:emit_signal("clientbutton::kill")
end)

-- client.connect_signal("focus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

-- client.connect_signal("unfocus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

