local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

maximizebutton = wibox.widget {
  image  = beautiful.titlebar_maximized_button_focus_active,
  resize = true,
  widget = wibox.widget.imagebox
}

maximizebutton:connect_signal("clientbutton::maximize", function()

                        local c = client.focus
                        if c then
                          if c.maximized then
                            c.maximized = false
                          else
                            c.maximized = true
                          end
                        end

end)

maximizebutton:connect_signal("button::press", function()
                        maximizebutton:emit_signal("clientbutton::maximize")
end)

-- client.connect_signal("focus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

-- client.connect_signal("unfocus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

