local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

local AutoHideTrigger = function(s, side_panel, side_panel_base, side_panel_border)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    awful.wibar(
      {
        ontop = true,
        screen = s,
        type = "utility",
        position = configuration.sidepanel_position,
        width = 1,
        height = 1,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = true,
        bg = beautiful.side_panel_trigger_bg,
        fg = beautiful.side_panel_trigger_fg,
        struts = {
          top = 0,
          bottom = 0,
          left = 0,
          right = 0,
        }
      }
    )

  -- Make sure the struts are always at 0
  function fixstruts()
    panel:struts(
      {
        top = 0,
        bottom = 0,
        left = 0,
        right = 0,
      }
    )

    side_panel:struts(
      {
        top = 0,
        bottom = 0,
        left = 0,
        right = 0,
      }
    )

    if side_panel_border ~= false then
      side_panel_border:struts(
        {
          top = 0,
          bottom = 0,
          left = 0,
          right = 0,
        }
      )
    end
  end

  -- panel:setup {
  --   layout = wibox.layout.align.horizontal,
  --   { -- Left widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     mylauncher,
  --   },
  --   s.mytasklist, -- Middle widget
  --   { -- Right widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     s.mylayoutbox,
  --   },
  -- }

  side_panel.visible = false

  panel:connect_signal("autohide::toggle", function()
                         if side_panel.visible == false then
                           if side_panel_border ~= false then
                             side_panel_border.visible = true
                           end
                           side_panel_base.visible = true
                           side_panel.visible = true
                           panel.visible = false
                           fixstruts()
                         else
                           if side_panel_border ~= false then
                             side_panel_border.visible = false
                           end
                           side_panel_base.visible = false
                           side_panel.visible = false
                           panel.visible = true
                           fixstruts()
                         end
  end)

  panel:connect_signal("mouse::enter", function()
                                     panel:emit_signal("autohide::toggle")
  end)

  side_panel:connect_signal("mouse::leave", function()
                             panel:emit_signal("autohide::toggle")
  end)

  return panel
end

return AutoHideTrigger

