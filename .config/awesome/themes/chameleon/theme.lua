---------------------------
-- Default awesome theme --
---------------------------

local gears = require("gears")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local gtk = require("beautiful.gtk")
local dpi = xresources.apply_dpi

local xrdb = xresources.get_current_theme()
local theme_gtk = gtk.get_theme_variables()


local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local theme = {}

theme.font          = "Ubuntu 8"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.systray_icon_spacing    = 6

-- Top panel
-- Modern Unity Topbar that follows GTK theme
theme.top_panel_bg   = theme_gtk.wm_bg_color
theme.top_panel_fg   = theme_gtk.fg_color
theme.bg_systray    = theme.top_panel_bg

-- The Classic Unity Topbar Gradient!
-- theme.top_panel_bg = { type = "linear",
--                       from = { 0, 0 },
--                       to = { 0, 25 },
--                       stops = { { 0, "#5a584f" }, { 1, "#403f3a" } }
-- }
-- theme.top_panel_fg   = "#f4f4f4"
-- theme.bg_systray    = "#5a584f"


theme.taglist_shape = gears.shape.circle
theme.taglist_bg_focus = xrdb.foreground .. "33"
theme.taglist_bg_occupied = xrdb.foreground .. "1A"

-- Hotkeys Popup

theme.hotkeys_bg =  xrdb.background .. "CC" -- Background color used for miscellaneous labels of hotkeys widget.

theme.hotkeys_modifiers_fg = "#f4f4f4" -- Foreground color used for hotkey modifiers (Ctrl, Alt, Super, etc).

theme.hotkeys_label_bg =  "#f4f4f4" -- Background color used for miscellaneous labels of hotkeys widget. 
theme.hotkeys_label_fg =  "#f4f4f4" -- Foreground color used for hotkey groups and other labels.

-- Side panel

theme.side_panel_bg  = xrdb.background .. "CC"
theme.side_panel_fg  = xrdb.foreground

theme.tasklist_bg_normal 	 = theme.side_panel_fg .. "4D" -- The default background color.
theme.tasklist_fg_normal   = theme.side_panel_fg .. "4D"	-- The default foreground (text) color.
theme.tasklist_bg_focus 	 = theme.side_panel_fg -- The focused client background color.
theme.tasklist_fg_focus 	 = theme.side_panel_fg -- The focused client foreground (text) color.
theme.tasklist_bg_urgent 	 = theme.side_panel_fg -- The urgent clients background color.
theme.tasklist_fg_urgent 	 = theme.side_panel_fg -- The urgent clients foreground (text) color.
theme.tasklist_bg_minimize = theme.side_panel_fg .. "1A"	-- The minimized clients background color.
theme.tasklist_fg_minimize = theme.side_panel_fg .. "1A"	-- The minimized clients foreground (text) color.

theme.side_panel_trigger_bg  = theme.bg_normal
theme.side_panel_trigger_fg  = theme.fg_normal

theme.side_panel_border_bg  = theme.fg_normal .. "40"
theme.side_panel_border_fg  = theme.fg_normal
--

-- Dash Panel

theme.dash_panel_bg  = xrdb.background .. "CC"
theme.dash_panel_fg  = xrdb.foreground


theme.useless_gap   = dpi(0)
theme.border_width  = dpi(0)
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

theme.top_panel_bg   = theme_gtk.wm_bg_color
theme.top_panel_fg   = theme_gtk.fg_color

theme.titlebar_fg_normal        = theme_gtk.fg_color	-- The titlebar foreground (text) color.
theme.titlebar_bg_normal      = theme_gtk.wm_bg_color	-- The titlebar background color.
-- theme.titlebar_bgimage_normal =  -- The titlebar background image image.
theme.titlebar_fg             =	 theme_gtk.fg_color -- The titlebar foreground (text) color.
theme.titlebar_bg 	          =  theme_gtk.wm_bg_color -- The titlebar background color.
-- theme.titlebar_bgimage 	      =  -- The titlebar background image image.
theme.titlebar_fg_focus 	    =  theme_gtk.fg_color -- The focused titlebar foreground (text) color.
theme.titlebar_bg_focus 	    =  theme_gtk.wm_bg_color -- The focused titlebar background color.
-- theme.titlebar_bgimage_focus 	=  -- The focused titlebar background image image.

theme.dash_launcher_icon = "~/.config/awesome/themes/chameleon/dash/launcher_bfb.png"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "~/.config/awesome/themes/chameleon/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "~/.config/awesome/themes/chameleon/titlebar/close_normal.png"
theme.titlebar_close_button_normal_press  = "~/.config/awesome/themes/chameleon/titlebar/close_press.png"
theme.titlebar_close_button_focus  = "~/.config/awesome/themes/chameleon/titlebar/close_focus.png"
theme.titlebar_close_button_focus_hover  = "~/.config/awesome/themes/chameleon/titlebar/close_focus_hover.png"
theme.titlebar_close_button_focus_press  = "~/.config/awesome/themes/chameleon/titlebar/close_press.png"

theme.titlebar_minimize_button_normal = "~/.config/awesome/themes/chameleon/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_normal_press = "~/.config/awesome/themes/chameleon/titlebar/minimize_press.png"
theme.titlebar_minimize_button_focus  = "~/.config/awesome/themes/chameleon/titlebar/minimize_focus.png"
theme.titlebar_minimize_button_focus_hover  = "~/.config/awesome/themes/chameleon/titlebar/minimize_focus_hover.png"
theme.titlebar_minimize_button_focus_press  = "~/.config/awesome/themes/chameleon/titlebar/minimize_press.png"

theme.titlebar_ontop_button_normal_inactive = "~/.config/awesome/themes/chameleon/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "~/.config/awesome/themes/chameleon/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "~/.config/awesome/themes/chameleon/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "~/.config/awesome/themes/chameleon/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "~/.config/awesome/themes/chameleon/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "~/.config/awesome/themes/chameleon/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "~/.config/awesome/themes/chameleon/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "~/.config/awesome/themes/chameleon/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "~/.config/awesome/themes/chameleon/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "~/.config/awesome/themes/chameleon/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "~/.config/awesome/themes/chameleon/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "~/.config/awesome/themes/chameleon/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "~/.config/awesome/themes/chameleon/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_normal_inactive_press = "~/.config/awesome/themes/chameleon/titlebar/maximized_press.png"

theme.titlebar_maximized_button_focus_inactive  = "~/.config/awesome/themes/chameleon/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_focus_inactive_hover  = "~/.config/awesome/themes/chameleon/titlebar/maximized_hover.png"
theme.titlebar_maximized_button_focus_inactive_press  = "~/.config/awesome/themes/chameleon/titlebar/maximized_press.png"

theme.titlebar_maximized_button_normal_active = "~/.config/awesome/themes/chameleon/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_normal_active_press = "~/.config/awesome/themes/chameleon/titlebar/maximized_press.png"

theme.titlebar_maximized_button_focus_active  = "~/.config/awesome/themes/chameleon/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_focus_active_hover  = "~/.config/awesome/themes/chameleon/titlebar/maximized_hover.png"
theme.titlebar_maximized_button_focus_active_press  = "~/.config/awesome/themes/chameleon/titlebar/maximized_hover.png"

-- theme.wallpaper = \"~/.config/awesome/themes/chameleon/background.png"

-- We'll follow the wallpaper the WPGTK is using
theme.wallpaper = "~/.config/wpg/.current"

-- You can use your own layout icons like this:
theme.layout_fairh = "~/.config/awesome/themes/chameleon/layouts/fairhw.png"
theme.layout_fairv = "~/.config/awesome/themes/chameleon/layouts/fairvw.png"
theme.layout_floating  = "~/.config/awesome/themes/chameleon/layouts/floatingw.png"
theme.layout_magnifier = "~/.config/awesome/themes/chameleon/layouts/magnifierw.png"
theme.layout_max = "~/.config/awesome/themes/chameleont/layouts/maxw.png"
theme.layout_fullscreen = "~/.config/awesome/themes/chameleon/layouts/fullscreenw.png"
theme.layout_tilebottom = "~/.config/awesome/themes/chameleon/layouts/tilebottomw.png"
theme.layout_tileleft   = "~/.config/awesome/themes/chameleon/layouts/tileleftw.png"
theme.layout_tile = "~/.config/awesome/themes/chameleon/layouts/tilew.png"
theme.layout_tiletop = "~/.config/awesome/themes/chameleon/layouts/tiletopw.png"
theme.layout_spiral  = "~/.config/awesome/themes/chameleon/layouts/spiralw.png"
theme.layout_dwindle = "~/.config/awesome/themes/chameleon/layouts/dwindlew.png"
theme.layout_cornernw = "~/.config/awesome/themes/chameleon/layouts/cornernww.png"
theme.layout_cornerne = "~/.config/awesome/themes/chameleon/layouts/cornernew.png"
theme.layout_cornersw = "~/.config/awesome/themes/chameleon/layouts/cornersww.png"
theme.layout_cornerse = "~/.config/awesome/themes/chameleon/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
